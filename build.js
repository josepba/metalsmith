
var Metalsmith  = require('metalsmith');
var collections = require('metalsmith-collections');
var layouts     = require('metalsmith-layouts');
var markdown    = require('metalsmith-markdown');
var permalinks  = require('metalsmith-permalinks');
var assets      = require('metalsmith-assets');


Metalsmith(__dirname)         // __dirname defined by node.js:
                              // name of current working directory
  .metadata({                 // add any variable you want
                              // use them in layout-files
    sitename:      "Kumori's Site",
    siteurl:       "http://www.kumori.cloud/",
    description:   "Kumori main corporate web site",
    generatorname: "Metalsmith",
    generatorurl:  "http://metalsmith.io/",
    copyright:     "Kumori Systems, S.L."
  })
  .source('./src')            // source directory
  .destination('./build')     // destination directory
  .clean(true)                // clean destination before
  .use(collections({          // group all blog posts by internally
    posts: 'posts/*.md'       // adding key 'collections':'posts'
  }))                         // use `collections.posts` in layouts
  .use(markdown())            // transpile all md into html
  .use(permalinks({           // change URLs to permalink URLs
    relative: false           // put css only in /css
  }))
  .use(layouts({
    engine : "nunjucks"
  }))
  .use(assets({
      source: "assets"
  }))
  .build(function(err) {      // build process
    if (err) throw err;       // error handling is required
  });